﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.AI;

public class TestingGetWorldPositionOfTheClick : MonoBehaviour
{
#pragma warning disable 0649
    [SerializeField] float speed = 2f;
    [SerializeField] Transform targetTransform;
    [SerializeField] Transform clickedPointTransform;
#pragma warning restore 0649

    Tween tweenPath;
    
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Clicked();
        }
    }
    
    void Clicked()
    {
        var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
 
        var hit = new RaycastHit();

        var layerMask = (1 << LayerMask.NameToLayer("Ground"));
 
        if (Physics.Raycast (ray, out hit, Mathf.Infinity, layerMask))
        {
            var origin = targetTransform.localPosition;
            var destiny = hit.point;

            var wayPoints = GetWayPoints(origin, destiny);

            if (wayPoints.Length == 0) { return; }
            
            var pathDuration = GetPathDuration(origin, wayPoints, speed);

            tweenPath?.Kill();
            tweenPath = targetTransform.DOLocalPath(wayPoints, pathDuration).SetEase(Ease.Linear);

            tweenPath.onWaypointChange += wayPointIndex =>
            {
                var nextWayPointIndex = wayPointIndex + 1;
                
                if (nextWayPointIndex >= wayPoints.Length) { return; }

                targetTransform.DOLookAt(wayPoints[nextWayPointIndex], 0.2f, AxisConstraint.Y);
            };
            
            Debug.LogFormat("NEW PATH!!!");
            
            foreach (var wayPoint in wayPoints)
            {
                Debug.LogFormat("[wayPoint: {0}]", wayPoint);
            }

            clickedPointTransform.localPosition = destiny;
        }
    }

    static Vector3[] GetWayPoints(Vector3 origin, Vector3 destiny)
    {
        var navMeshPath = new NavMeshPath();

        NavMesh.CalculatePath(origin, destiny, NavMesh.AllAreas, navMeshPath);

        return navMeshPath.corners;
    }

    static float GetPathDuration(Vector3 origin, Vector3[] wayPoints, float speed)
    {
        var pathDistance = GetPathDistance(origin, wayPoints);

        return pathDistance / speed;
    }

    static float GetPathDistance(Vector3 origin, Vector3[] wayPoints)
    {
        var pathDistance = 0f;

        for (int i = 0; i < wayPoints.Length; i++)
        {
            if (i == 0)
            {
                pathDistance += Vector3.Distance(wayPoints[i], origin);
            }
            else
            {
                pathDistance += Vector3.Distance(wayPoints[i], wayPoints[i - 1]);
            }
        }

        return pathDistance;
    }
}
