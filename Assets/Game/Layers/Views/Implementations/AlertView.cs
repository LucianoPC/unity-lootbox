using Game.Layers;

namespace Game.Views
{
    public class AlertView : GameArch.Presenters.Connection.IAlertView, GameArch.Presenters.Authentication.IAlertView
    {
        readonly AlertPopup alertPopup;

        public AlertView(AlertPopup alertPopup)
        {
            this.alertPopup = alertPopup;
        }

        public void OnInitialize()
        {
        }

        public void OnFinish()
        {
        }

        public void Show(string message)
        {
            alertPopup.Show(message);
        }

        public void Hide()
        {
            alertPopup.Hide();
        }
    }
}