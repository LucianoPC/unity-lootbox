using Game.Layers;
using GameArch;
using GameArch.Box;
using UnityEngine;

namespace Game.Views
{
    public class AlertViewBox : BaseViewBox
    {
#pragma warning disable 0649
        [SerializeField] AlertPopup alertPopup;
#pragma warning restore 0649

        AlertView alertView;

        public override IView View => alertView ?? (alertView = new AlertView(alertPopup));
    }
}