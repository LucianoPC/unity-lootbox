using System;
using Game.Layers;

namespace Game.Views
{
    public class DialogView : GameArch.Presenters.Connection.IDialogView, GameArch.Presenters.Authentication.IDialogView
    {
        readonly DialogPopup dialogPopup;

        public DialogView(DialogPopup dialogPopup)
        {
            this.dialogPopup = dialogPopup;
        }

        public void OnInitialize()
        {
        }

        public void OnFinish()
        {
        }

        public void Show(string message, string confirmationText, Action onConfirm)
        {
            dialogPopup.Show(message, confirmationText, onConfirm);
        }

        public void Hide()
        {
            dialogPopup.Hide();
        }
    }
}