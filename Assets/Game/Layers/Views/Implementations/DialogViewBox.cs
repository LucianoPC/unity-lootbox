using Game.Layers;
using GameArch;
using GameArch.Box;
using UnityEngine;

namespace Game.Views
{
    public class DialogViewBox : BaseViewBox
    {
#pragma warning disable 0649
        [SerializeField] DialogPopup dialogPopup;
#pragma warning restore 0649

        DialogView dialogView;

        public override IView View => dialogView ?? (dialogView = new DialogView(dialogPopup));
    }
}