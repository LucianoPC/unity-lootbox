using GameArch;
using GameArch.Controllers.Authentication;
using GameArch.Controllers.Connection;

namespace Game.Managers
{
    public class GameManager : BaseGameManager
    {
        readonly IGameManagerInfo info;
        
        ConnectionController connectionController;
        AuthenticationController authenticationController;

        public GameManager(IGameManagerInfo info)
        {
            this.info = info;
        }

        public GameManager(IGameManagerInfo info, IConnectionsCreator connectionsCreator,
            IControllersCreator controllersCreator) : base(connectionsCreator, controllersCreator)
        {
            this.info = info;
        }

        protected override void OnInitialize()
        {
            connectionController = GetController<ConnectionController>(info.ConnectionControllerId);
            authenticationController = GetController<AuthenticationController>(info.AuthenticationControllerId);
            
            RegisterCallbacks();
        }

        protected override void OnAfterInitialize()
        {
            Connect();
        }

        protected override void OnFinish()
        {
            UnRegisterCallbacks();
        }

        protected override void OnApplicationQuit()
        {
            Disconnect();
        }
        
        void RegisterCallbacks()
        {
            connectionController.ConnectionSucceeded += OnConnectionSucceeded;
        }

        void UnRegisterCallbacks()
        {
            connectionController.ConnectionSucceeded -= OnConnectionSucceeded;
        }

        void Connect()
        {
            if (!connectionController.IsConnected)
            {
                connectionController.Connect();
            }
            else
            {
                Authenticate();
            }
        }

        void OnConnectionSucceeded()
        {
            Authenticate();
        }

        void Authenticate()
        {
            if (!authenticationController.IsAuthenticated)
            {
                authenticationController.Authenticate();
            }
        }

        void Disconnect()
        {
            connectionController.Disconnect();
        }
    }
}