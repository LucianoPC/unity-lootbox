using System;
using GameArch;
using GameArch.Box;
using GameArch.Controllers.Authentication.Box;
using GameArch.Controllers.Connection.Box;
using UnityEngine;

namespace Game.Managers
{
    [CreateAssetMenu(fileName = "GameManager", menuName = "GameArch Boxes/Managers/Game Manager", order = 0)]
    public class GameManagerBox : BaseGameManagerBox
    {
#pragma warning disable 0649
        [SerializeField] GameManagerInfo gameManagerInfo;
#pragma warning restore 0649
        
        protected override BaseGameManager CreateGameManager()
        {
            return new GameManager(gameManagerInfo);
        }

        
        [Serializable]
        class GameManagerInfo : IGameManagerInfo
        {
#pragma warning disable 0649
            [Header("Identifiers")]
            [SerializeField] ConnectionControllerBox connectionController;
            [SerializeField] AuthenticationControllerBox authenticationController;
#pragma warning restore 0649
            
            public string ConnectionControllerId => connectionController.Id;
            public string AuthenticationControllerId => authenticationController.Id;
        }
    }
}