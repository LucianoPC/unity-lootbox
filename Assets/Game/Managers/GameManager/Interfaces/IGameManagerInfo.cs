namespace Game.Managers
{
    public interface IGameManagerInfo
    {
        string ConnectionControllerId { get; }
        string AuthenticationControllerId { get; }
    }
}