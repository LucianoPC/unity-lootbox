using Game.Managers.SceneManagers.MainMenu;
using GameArch;
using GameArch.Controllers.Authentication;
using UnityEngine;

namespace Game.Managers
{
    public class MainMenuSceneManager : BaseSceneManager
    {
        readonly IMainMenuSceneManagerInfo info;
        
        AuthenticationController authenticationController;

        public MainMenuSceneManager(IMainMenuSceneManagerInfo info)
        {
            this.info = info;
        }

        public MainMenuSceneManager(IMainMenuSceneManagerInfo info, BaseGameManager gameManager,
            IConnectionsCreator connectionsCreator, IControllersCreator controllersCreator,
            IPresentersCreator presentersCreator, IViewsCreator viewsCreator) : 
            base(gameManager, connectionsCreator, controllersCreator, presentersCreator, viewsCreator)
        {
            this.info = info;
        }

        protected override void OnInitialize()
        {
            authenticationController = GetController<AuthenticationController>(info.AuthenticationControllerId);
            
            RegisterCallbacks();
        }

        protected override void OnFinish()
        {
            UnRegisterCallbacks();
        }

        void RegisterCallbacks()
        {
            authenticationController.AuthenticationSucceeded += OnAuthenticationSucceeded;
        }

        void UnRegisterCallbacks()
        {
            authenticationController.AuthenticationSucceeded -= OnAuthenticationSucceeded;
        }

        void OnAuthenticationSucceeded(AuthenticationData authenticationData)
        {
            Debug.LogFormat("MainMenuSceneManager OnAuthenticationSucceeded");
        }
    }
}