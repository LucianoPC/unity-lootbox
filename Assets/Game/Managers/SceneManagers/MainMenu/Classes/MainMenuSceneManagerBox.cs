using System;
using Game.Managers.SceneManagers.MainMenu;
using GameArch;
using GameArch.Box;
using GameArch.Controllers.Authentication.Box;
using UnityEngine;

namespace Game.Managers
{
    public class MainMenuSceneManagerBox : BaseSceneManagerBox
    {
#pragma warning disable 0649
        [SerializeField] MainMenuSceneManagerInfo menuSceneManagerInfo;
#pragma warning restore 0649
        
        protected override BaseSceneManager CreateSceneManager()
        {
            return new MainMenuSceneManager(menuSceneManagerInfo);
        }
        
        
        [Serializable]
        class MainMenuSceneManagerInfo : IMainMenuSceneManagerInfo
        {
#pragma warning disable 0649
            [Header("Identifiers")]
            [SerializeField] AuthenticationControllerBox authenticationController;
#pragma warning restore 0649
            
            public string AuthenticationControllerId => authenticationController.Id;
        }
    }
}