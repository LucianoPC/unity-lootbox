namespace Game.Managers.SceneManagers.MainMenu
{
    public interface IMainMenuSceneManagerInfo
    {
        string AuthenticationControllerId { get; }
    }
}