﻿using UnityEditor;
using UnityEngine;

namespace Game.Tools
{
    public static class Clear
    {
        [MenuItem("Tools/Game/Clear/Clear Player Prefs")]
        public static void ClearPlayerPrefs()
        {
            PlayerPrefs.DeleteAll();
        }
    }
}
