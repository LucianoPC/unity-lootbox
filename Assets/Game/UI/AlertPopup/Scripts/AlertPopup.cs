﻿using UnityEngine;
using UnityEngine.UI;

namespace Game.Layers
{
    public class AlertPopup : MonoBehaviour
    {
#pragma warning disable 0649
        [SerializeField] Text messageText;
#pragma warning restore 0649

        Data data;

        public Data CurrentData
        {
            get => data;
            set => SetData(value);
        }

        public void Show(string message)
        {
            var newData = CurrentData;

            newData.Message = message;
            newData.Show = true;

            CurrentData = newData;
        }

        public void Hide()
        {
            var newData = CurrentData;

            newData.Show = false;

            CurrentData = newData;
        }

        void SetData(Data data)
        {
            this.data = data;
            
            SetupMessageText();
            SetupVisibility();
        }

        void SetupMessageText()
        {
            messageText.text = data.Message;
        }

        void SetupVisibility()
        {
            var isActive = gameObject.activeSelf;
            var needChangeActive = isActive != data.Show;

            if (needChangeActive)
            {
                gameObject.SetActive(data.Show);
            }
        }

        public struct Data
        {
            public bool Show;
            public string Message;
        }
    }
}
