﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Game.Layers
{
    public class DialogPopup : MonoBehaviour
    {
#pragma warning disable 0649
        [SerializeField] Text messageText;
        [SerializeField] Text buttonText;
        [SerializeField] Button confirmButton;
#pragma warning restore 0649
        
        Data data;

        public Data CurrentData
        {
            get => data;
            set => SetData(value);
        }

        public Action ConfirmPressed;

        void Start()
        {
            confirmButton.onClick.AddListener(NotifyConfirmPressed);
        }

        void OnDestroy()
        {
            confirmButton.onClick.RemoveListener(NotifyConfirmPressed);
        }
        
        public void Show(string message, string buttonText, Action onConfirm)
        {
            var newData = CurrentData;

            newData.Message = message;
            newData.ButtonText = buttonText;
            newData.Show = true;

            CurrentData = newData;

            ConfirmPressed = onConfirm;
        }

        public void Hide()
        {
            var newData = CurrentData;

            newData.Show = false;

            CurrentData = newData;

            ConfirmPressed = null;
        }

        void SetData(Data data)
        {
            this.data = data;
            
            SetupMessageText();
            SetupButtonText();
            SetupVisibility();
        }

        void SetupMessageText()
        {
            messageText.text = data.Message;
        }

        void SetupButtonText()
        {
            buttonText.text = data.ButtonText;
        }

        void SetupVisibility()
        {
            var isActive = gameObject.activeSelf;
            var needChangeActive = isActive != data.Show;

            if (needChangeActive)
            {
                gameObject.SetActive(data.Show);
            }
        }

        void NotifyConfirmPressed()
        {
            ConfirmPressed?.Invoke();
        }

        public struct Data
        {
            public bool Show;
            public string Message;
            public string ButtonText;
        }
    }
}
