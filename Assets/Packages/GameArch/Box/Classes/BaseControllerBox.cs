using UnityEngine;

namespace GameArch.Box
{
    public abstract class BaseControllerBox : ScriptableObject
    {
#pragma warning disable 0649
        [SerializeField, ReadOnly] string id = IdGenerator.NewId();
#pragma warning restore 0649

        public string Id => id;
        
        public abstract BaseController Controller { get; }

#if UNITY_EDITOR
        [ContextMenu("Generate Id")]
        void GenerateId()
        {
            id = IdGenerator.NewId();
        }
#endif
    }
}