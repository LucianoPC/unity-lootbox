using UnityEngine;

namespace GameArch.Box
{
    public abstract class BaseGameManagerBox : ScriptableObject
    {
#pragma warning disable 0649
        [SerializeField] GameArch.Box.ConnectionsCreator connections;
        [SerializeField] GameArch.Box.ControllersCreator controllers;
#pragma warning restore 0649

        protected abstract BaseGameManager CreateGameManager();
        
        public BaseGameManager GameManager
        {
            get
            {
                if (!BaseGameManager.IsInitialized)
                {
                    var gameManager = CreateGameManager();
                    gameManager.BaseInitialization(connections, controllers);
                    
                    BaseGameManager.Set(gameManager);
                }
                
                return BaseGameManager.Instance;
            }
        }
    }
}