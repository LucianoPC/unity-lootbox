using UnityEngine;

namespace GameArch.Box
{
    public abstract class BaseSceneManagerBox : MonoBehaviour
    {
#pragma warning disable 0649
        [SerializeField] GameArch.Box.BaseGameManagerBox gameManager;
        [SerializeField] GameArch.Box.ConnectionsCreator connections;
        [SerializeField] GameArch.Box.ControllersCreator controllers;
        [SerializeField] GameArch.Box.PresentersCreator presenters;
        [SerializeField] GameArch.Box.ViewsCreator views;
#pragma warning restore 0649

        BaseSceneManager sceneManager;

        protected abstract BaseSceneManager CreateSceneManager();

        void Awake()
        {
            if (sceneManager != null) return;

            sceneManager = CreateSceneManager();
            sceneManager.BaseInitialization(gameManager.GameManager, connections, controllers, presenters, views);
        }
        
        void Start()
        {
            sceneManager.Initialize();
        }

        void OnDestroy()
        {
            sceneManager.Finish();
        }

        void OnApplicationQuit()
        {
            sceneManager.ApplicationQuit();
        }
    }
}