using System;
using UnityEngine;

namespace GameArch.Box
{
    [Serializable]
    public class ConnectionsCreator : IConnectionsCreator
    {
#pragma warning disable 0649
        [SerializeField] BaseConnectionBox[] list;
#pragma warning restore 0649
        
        public ConnectionCreateInfo[] CreateConnections()
        {
            var infos = new ConnectionCreateInfo[list.Length];

            for (var i = 0; i < list.Length; i++)
            {
                var connectionBox = list[i];
                var info = new ConnectionCreateInfo(connectionBox.Id, connectionBox.Connection);

                infos[i] = info;
            }

            return infos;
        }
    }
}