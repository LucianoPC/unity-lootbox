using System;
using UnityEngine;

namespace GameArch.Box
{
    [Serializable]
    public class ControllersCreator : IControllersCreator
    {
#pragma warning disable 0649
        [SerializeField] BaseControllerBox[] list;
#pragma warning restore 0649
        
        public ControllerCreateInfo[] CreateControllers()
        {
            var infos = new ControllerCreateInfo[list.Length];

            for (var i = 0; i < list.Length; i++)
            {
                var controllerBox = list[i];
                var info = new ControllerCreateInfo(controllerBox.Id, controllerBox.Controller);

                infos[i] = info;
            }

            return infos;
        }
    }
}