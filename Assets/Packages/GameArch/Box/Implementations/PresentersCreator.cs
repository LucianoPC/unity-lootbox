using System;
using UnityEngine;

namespace GameArch.Box
{
    [Serializable]
    public class PresentersCreator : IPresentersCreator
    {
#pragma warning disable 0649
        [SerializeField] BasePresenterBox[] list;
#pragma warning restore 0649
        
        public PresenterCreateInfo[] CreatePresenters()
        {
            var infos = new PresenterCreateInfo[list.Length];

            for (var i = 0; i < list.Length; i++)
            {
                var presenterBox = list[i];
                var info = new PresenterCreateInfo(presenterBox.Id, presenterBox.Presenter);

                infos[i] = info;
            }

            return infos;
        }
    }
}