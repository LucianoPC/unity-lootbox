using System;
using UnityEngine;

namespace GameArch.Box
{
    [Serializable]
    public class ViewsCreator : IViewsCreator
    {
#pragma warning disable 0649
        [SerializeField] BaseViewBox[] list;
#pragma warning restore 0649
        
        public ViewCreateInfo[] CreateViews()
        {
            var infos = new ViewCreateInfo[list.Length];

            for (var i = 0; i < list.Length; i++)
            {
                var viewBox = list[i];
                var info = new ViewCreateInfo(viewBox.Id, viewBox.View);

                infos[i] = info;
            }

            return infos;
        }
    }
}