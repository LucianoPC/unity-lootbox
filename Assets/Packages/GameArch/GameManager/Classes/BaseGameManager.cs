namespace GameArch
{
    public abstract class BaseGameManager
    {
        static BaseGameManager instance;
        
        public static BaseGameManager Instance => instance;

        public static bool IsInitialized => instance != null;

        public static void Set<T>(T gameManager) where T : BaseGameManager
        {
            if (instance != null) { return; }

            instance = gameManager;
        }

        public static void Create<T>(IConnectionsCreator connectionsCreator, IControllersCreator controllersCreator) where T : BaseGameManager, new()
        {
            if (instance != null) { return; }
            
            instance = new T();
            instance.BaseInitialization(connectionsCreator, controllersCreator);
        }
        
        IConnectionsCreator connectionsCreator;
        IControllersCreator controllersCreator;
        
        readonly ConnectionResources connectionResources;
        readonly ControllerResources controllerResources;

        bool initialized;
        bool baseInitialized;

        protected BaseGameManager(IConnectionsCreator connectionsCreator, IControllersCreator controllersCreator) : this()
        {
            BaseInitialization(connectionsCreator, controllersCreator);
        }

        protected BaseGameManager()
        {
            connectionResources = new ConnectionResources();
            controllerResources = new ControllerResources();
        }

        public ConnectionResources ConnectionResources => connectionResources;

        public ControllerResources ControllerResources => controllerResources;
        
        protected abstract void OnInitialize();
        protected abstract void OnAfterInitialize();
        protected abstract void OnFinish();
        protected abstract void OnApplicationQuit();

        public void BaseInitialization(IConnectionsCreator connectionsCreator, IControllersCreator controllersCreator)
        {
            if (baseInitialized) { return; }

            baseInitialized = true;
            
            this.connectionsCreator = connectionsCreator;
            this.controllersCreator = controllersCreator;
        }
        
        public void Initialize()
        {
            if (initialized) { return; }

            initialized = true;
            
            SetupConnections();
            SetupControllers();
            
            OnInitialize();
        }

        public void AfterInitialize()
        {
            OnAfterInitialize();
        }
        
        public void Finish()
        {
            if (!initialized) { return; }

            initialized = false;
            
            OnFinish();
        
            FinishControllers();
            FinishConnections();
        }

        public void ApplicationQuit()
        {
            OnApplicationQuit();
        }

        protected Connection GetConnection(string id)
        {
            return connectionResources.Get(id);
        }
        
        protected T GetController<T>(string id) where T : BaseController
        {
            return controllerResources.Get<T>(id);
        }

        void SetupConnections()
        {
            var connectionCreateInfos = connectionsCreator.CreateConnections();

            foreach (var info in connectionCreateInfos)
            {
                connectionResources.Add(info.Id, info.Connection);
            }
        }

        void SetupControllers()
        {
            var controllerCreateInfos = controllersCreator.CreateControllers();

            foreach (var info in controllerCreateInfos)
            {
                info.Controller.BaseInitialization(connectionResources);
                info.Controller.Initialize();
                
                controllerResources.Add(info.Id, info.Controller);
            }
        }
        
        void FinishConnections()
        {
            var connections = connectionResources.GetAll();

            foreach (var connection in connections)
            {
                connection.ClearAll();
            }
        }

        void FinishControllers()
        {
            var controllers = controllerResources.GetAll();

            foreach (var controller in controllers)
            {
                controller.Finish();
            }
        }
    }
}