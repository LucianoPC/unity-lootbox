namespace GameArch
{
    public abstract class BaseSceneManager
    {
        BaseGameManager gameManager;
        
        IConnectionsCreator connectionsCreator;
        IControllersCreator controllersCreator;
        IPresentersCreator presentersCreator;
        IViewsCreator viewsCreator;
        
        readonly ConnectionResources connectionResources;
        readonly ControllerResources controllerResources;
        readonly PresenterResources presenterResources;
        readonly ViewResources viewResources;
        
        ConnectionResources connectionResourcesFull;
        ControllerResources controllerResourcesFull;
        
        bool initialized;
        bool baseInitialized;

        protected BaseSceneManager()
        {
            connectionResources = new ConnectionResources();
            controllerResources = new ControllerResources();
            presenterResources = new PresenterResources();
            viewResources = new ViewResources();
        }

        protected BaseSceneManager(BaseGameManager gameManager, 
            IConnectionsCreator connectionsCreator,
            IControllersCreator controllersCreator,
            IPresentersCreator presentersCreator, 
            IViewsCreator viewsCreator) : this()
        {
            BaseInitialization(gameManager, connectionsCreator, controllersCreator, presentersCreator, viewsCreator);
        }

        protected abstract void OnInitialize();
        protected abstract void OnFinish();
        protected virtual void OnApplicationQuit() { } 
        
        public void BaseInitialization (BaseGameManager gameManager, 
            IConnectionsCreator connectionsCreator,
            IControllersCreator controllersCreator,
            IPresentersCreator presentersCreator, 
            IViewsCreator viewsCreator)
        {
            if (baseInitialized) { return; }

            baseInitialized = true;

            this.gameManager = gameManager;
            this.connectionsCreator = connectionsCreator;
            this.controllersCreator = controllersCreator;
            this.presentersCreator = presentersCreator;
            this.viewsCreator = viewsCreator;
        }
        
        public void Initialize()
        {
            if (initialized) { return; }

            initialized = true;
            
            gameManager.Initialize();
            
            SetupConnections();
            SetupControllers();
            SetupViews();
            SetupPresenters();
            
            OnInitialize();
            
            gameManager.AfterInitialize();
        }

        public void Finish()
        {
            OnFinish();
            
            ClearConnections();
        
            FinishPresenters();
            FinishViews();
            FinishControllers();
            FinishConnections();
        }

        public void ApplicationQuit()
        {
            gameManager.Finish();
            gameManager.ApplicationQuit();
            
            OnApplicationQuit();
        }

        protected Connection GetConnection(string connectionId)
        {
            return connectionResourcesFull.Get(connectionId);
        }
        
        protected T GetController<T>(string id) where T : BaseController
        {
            return controllerResourcesFull.Get<T>(id);
        }

        protected T GetPresenter<T>(string id) where T : BasePresenter
        {
            return presenterResources.Get<T>(id);
        }

        protected T GetView<T>(string id) where T : IView
        {
            return viewResources.Get<T>(id);
        }
        
        void SetupConnections()
        {
            var connectionCreateInfos = connectionsCreator.CreateConnections();

            foreach (var info in connectionCreateInfos)
            {
                connectionResources.Add(info.Id, info.Connection);
            }

            connectionResourcesFull = ConnectionResources.Merge(gameManager.ConnectionResources, connectionResources);
        }

        void SetupControllers()
        {
            var controllerCreateInfos = controllersCreator.CreateControllers();

            foreach (var info in controllerCreateInfos)
            {
                info.Controller.BaseInitialization(connectionResourcesFull);
                info.Controller.Initialize();
                
                controllerResources.Add(info.Id, info.Controller);
            }

            controllerResourcesFull = ControllerResources.Merge(gameManager.ControllerResources, controllerResources);
        }

        void SetupPresenters()
        {
            var presenterCreateInfos = presentersCreator.CreatePresenters();

            foreach (var info in presenterCreateInfos)
            {
                info.Presenter.BaseInitialization(viewResources, controllerResourcesFull);
                info.Presenter.Initialize();
                
                presenterResources.Add(info.Id, info.Presenter);
            }
        }
        
        void SetupViews()
        {
            var viewCreateInfo = viewsCreator.CreateViews();

            foreach (var info in viewCreateInfo)
            {
                viewResources.Add(info.Id, info.View);
            }
        }

        void ClearConnections()
        {
            var connections = connectionResources.GetAll();

            foreach (var connection in connections)
            {
                connection.ClearAll();
            }
        }
        
        void FinishConnections()
        {
            var connections = connectionResources.GetAll();

            foreach (var connection in connections)
            {
                connection.ClearAll();
            }
        }

        void FinishControllers()
        {
            var controllers = controllerResources.GetAll();

            foreach (var controller in controllers)
            {
                controller.Finish();
            }
        }

        void FinishPresenters()
        {
            var presenters = presenterResources.GetAll();

            foreach (var presenter in presenters)
            {
                presenter.Finish();
            }
        }

        void FinishViews()
        {
            
        }
    }
}