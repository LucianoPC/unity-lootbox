namespace GameArch
{
    public class EmptyConnectionsCreator : IConnectionsCreator
    {
        public ConnectionCreateInfo[] CreateConnections()
        {
            return new ConnectionCreateInfo[0];
        }
    }
}