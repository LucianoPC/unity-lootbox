namespace GameArch
{
    public class EmptyControllersCreator : IControllersCreator
    {
        public ControllerCreateInfo[] CreateControllers()
        {
            return new ControllerCreateInfo[0];
        }
    }
}