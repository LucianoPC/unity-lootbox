namespace GameArch
{
    public class EmptyPresentersCreator : IPresentersCreator
    {
        public PresenterCreateInfo[] CreatePresenters()
        {
            return new PresenterCreateInfo[0];
        }
    }
}