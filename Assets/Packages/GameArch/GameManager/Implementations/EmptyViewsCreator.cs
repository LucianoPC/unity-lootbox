namespace GameArch
{
    public class EmptyViewsCreator : IViewsCreator
    {
        public ViewCreateInfo[] CreateViews()
        {
            return new ViewCreateInfo[0];
        }
    }
}