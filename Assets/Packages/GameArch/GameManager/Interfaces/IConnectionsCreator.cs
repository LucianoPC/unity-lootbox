namespace GameArch
{
    public interface IConnectionsCreator
    {
        ConnectionCreateInfo[] CreateConnections();
    }
}