namespace GameArch
{
    public interface IControllersCreator
    {
        ControllerCreateInfo[] CreateControllers();
    }
}