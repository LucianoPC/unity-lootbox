namespace GameArch
{
    public interface IPresentersCreator
    {
        PresenterCreateInfo[] CreatePresenters();
    }
}