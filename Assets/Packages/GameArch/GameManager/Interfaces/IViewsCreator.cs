namespace GameArch
{
    public interface IViewsCreator
    {
        ViewCreateInfo[] CreateViews();
    }
}