namespace GameArch
{
    public class ConnectionCreateInfo
    {
        public string Id { private set; get; }
        public Connection Connection { private set; get; }

        public ConnectionCreateInfo(string id, Connection connection)
        {
            Id = id;
            Connection = connection;
        }
    }
}