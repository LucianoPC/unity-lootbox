namespace GameArch
{
    public class ControllerCreateInfo
    {
        public string Id { private set; get; }
        public BaseController Controller { private set; get; }

        public ControllerCreateInfo(string id, BaseController controller)
        {
            Id = id;
            Controller = controller;
        }
    }
}