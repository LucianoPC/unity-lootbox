namespace GameArch
{
    public class PresenterCreateInfo
    {
        public string Id { private set; get; }
        public BasePresenter Presenter { private set; get; }

        public PresenterCreateInfo(string id, BasePresenter presenter)
        {
            Id = id;
            Presenter = presenter;
        }
    }
}