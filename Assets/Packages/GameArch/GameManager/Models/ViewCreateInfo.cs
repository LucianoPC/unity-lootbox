namespace GameArch
{
    public class ViewCreateInfo
    {
        public string Id { private set; get; }
        public IView View { private set; get; }

        public ViewCreateInfo(string id, IView view)
        {
            Id = id;
            View = view;
        }
    }
}