﻿using System;

namespace GameArch
{
    public class Connection
    {
        public enum State
        {
            Connected,
            Disconnected,
        }

        readonly IConnector connector;
        readonly IRequester requester;
        readonly IListener listener;
        readonly IJsonSerializer jsonSerializer;

        readonly JsonConverterMap jsonConverterMap;
        readonly ConnectionStateController stateController;

        public bool IsConnected => stateController.CurrentState == State.Connected;
        
        public event Action<StateChangedData> StateChanged
        {
            add => stateController.StateChanged += value;
            remove => stateController.StateChanged += value;
        }

        public Connection(IConnector connector, IRequester requester, IListener listener,
            IJsonSerializer jsonSerializer)
        {
            this.connector = connector;
            this.requester = requester;
            this.listener = listener;
            this.jsonSerializer = jsonSerializer;

            jsonConverterMap = new JsonConverterMap();
            stateController = new ConnectionStateController(State.Disconnected);
            
            jsonConverterMap.AddConverter(NullObjectModelJsonConverter.FromJson);
        }

        public void Connect(string host, int port, Action onSuccess, Action<Error> onError)
        {
            if (IsConnected)
            {
                onSuccess?.Invoke();
                return;
            }
            
            connector.Connect(host, port, 
                onSuccess: () =>
                {
                    onSuccess?.Invoke();
                    stateController.SetState(State.Connected);
                }, 
                onError: error =>
                {
                    onError?.Invoke(error);
                    stateController.SetState(State.Disconnected);
                });
        }

        public void Disconnect()
        {
            if (!IsConnected) { return; }
            
            connector.Disconnect();
            stateController.SetState(State.Disconnected);
        }

        public void AddConverter<T>(Func<string, T> converter) where T : class
        {
            jsonConverterMap.AddConverter(converter);
        }

        public void SendRequest(string route, Action onSuccess, Action<Error> onError)
        {
            SendRequest(route, null, onSuccess, onError);
        }

        public void SendRequest(string route, object request, Action onSuccess, Action<Error> onError)
        {
            Action<NullObjectModel> actionSuccess = _ => { onSuccess?.Invoke(); };

            SendRequest(route, request, actionSuccess, onError);
        }
        
        public void SendRequest<T>(string route, Action<T> onSuccess, Action<Error> onError) 
            where T : class
        {
            SendRequest(route, null, onSuccess, onError);
        }

        public void SendRequest<T>(string route, object request, Action<T> onSuccess, Action<Error> onError) 
            where T : class
        {
            var requestData = request == null ? string.Empty : jsonSerializer.Serialize(request);
            
            requester.SendRequest(route, requestData,
                onSuccess: responsePayload => { SendRequestOnSuccess(responsePayload, onSuccess); },
                onError: errorPayload => { SendRequestOnError(errorPayload, onError); });
        }

        void SendRequestOnSuccess<T>(ResponsePayload responsePayload, Action<T> onSuccess) where T : class
        {
            var response = responsePayload.ToModel();

            var model = jsonConverterMap.Convert<T>(response.Data);
            
            onSuccess?.Invoke(model);
        }

        void SendRequestOnError(ErrorPayload errorPayload, Action<Error> onError)
        {
            var error = errorPayload.ToModel();
            
            onError?.Invoke(error);
        }

        public void ListenRoute(string route, Action callback)
        {
            Action<NullObjectModel> actionCallback = _ => { callback?.Invoke(); };

            ListenRoute(route, actionCallback);
        }

        public void ListenRoute<T>(string route, Action<T> callback) where T : class
        {
            listener.ListenRoute(route,
                callback: responsePayload =>
                {
                    var response = responsePayload.ToModel();
                    
                    var model = jsonConverterMap.Convert<T>(response.Data);
                    
                    callback?.Invoke(model);
                });
        }

        public void StopListenRoute(string route)
        {
            listener.StopListenRoute(route);
        }

        public void ClearRequests()
        {
            requester.ClearAllCallbacks();
        }

        public void StopListenAllRoutes()
        {
            listener.StopListenAllRoutes();
        }

        public void ClearAll()
        {
            ClearRequests();
            StopListenAllRoutes();
        }
        
        public class StateChangedData
        {
            public State OldState;
            public State NewState;

            public StateChangedData(State oldState, State newState)
            {
                OldState = oldState;
                NewState = newState;
            }
        }
    }
}
