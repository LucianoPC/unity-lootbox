namespace GameArch
{
    public class ConnectionResources : Resources<Connection>
    {
        public static ConnectionResources Merge(ConnectionResources a, ConnectionResources b)
        {
            return Resources<Connection>.Merge(a, b);
        }
    }
}