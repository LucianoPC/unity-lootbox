using System;

namespace GameArch
{
    public class ConnectionStateController
    {
        Connection.State currentState;
        
        public event Action<Connection.StateChangedData> StateChanged;

        public Connection.State CurrentState => currentState;

        public ConnectionStateController(Connection.State currentState)
        {
            this.currentState = currentState;
        }

        public void SetState(Connection.State newState)
        {
            var stateChanged = newState != currentState;
            
            if (!stateChanged) { return; }
            
            var oldState = currentState;
            currentState = newState;
            
            NotifyStateChanged(new Connection.StateChangedData(oldState, newState));
        }

        void NotifyStateChanged(Connection.StateChangedData stateChangedData)
        {
            StateChanged?.Invoke(stateChangedData);
        }
    }
}