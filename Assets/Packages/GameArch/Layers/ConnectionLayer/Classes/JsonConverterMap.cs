using System;
using System.Collections.Generic;
using UnityEngine;

namespace GameArch
{
    public class JsonConverterMap
    {
        Dictionary<string, object> map;

        public JsonConverterMap()
        {
            map = new Dictionary<string, object>();
        }

        public void AddConverter<T>(Func<string, T> converter) where T : class
        {
            var className = typeof(T).Name;
            var converterObject = converter;

            if (map.ContainsKey(className)) { return; }
            
            map.Add(className, converterObject);
        }

        public T Convert<T>(string json) where T : class
        {
            var className = typeof(T).Name;

            if (!map.ContainsKey(className))
            {
                Debug.LogErrorFormat("JsonConverterMap Convert: There is NOT a function to convert a string json into {0}", className);
                return null;
            }
            
            var converter = (Func<string, T>)map[className];

            return converter(json);
        }
    }
}