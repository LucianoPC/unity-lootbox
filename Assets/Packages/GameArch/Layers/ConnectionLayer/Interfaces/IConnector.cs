using System;

namespace GameArch
{
    public interface IConnector
    {
        void Connect(string host, int port, Action onSuccess, Action<Error> onError);
        void Disconnect();
    }
}