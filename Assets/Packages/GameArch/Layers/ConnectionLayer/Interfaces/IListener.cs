using System;

namespace GameArch
{
    public interface IListener
    {
        void ListenRoute(string route, Action<ResponsePayload> callback);
        
        void StopListenRoute(string route);
        void StopListenAllRoutes();
    }
}