using System;

namespace GameArch
{
    public interface IRequester
    {
        void SendRequest(string route, string requestData, Action<ResponsePayload> onSuccess, 
            Action<ErrorPayload> onError);

        void ClearAllCallbacks();
    }
}