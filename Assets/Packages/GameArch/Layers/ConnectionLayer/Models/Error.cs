namespace GameArch
{
    public class Error
    {
        public string Code { private set; get; }
        public string Message { private set; get; }
        
        public Error(string code, string message)
        {
            Code = code;
            Message = message;
        }
    }
}