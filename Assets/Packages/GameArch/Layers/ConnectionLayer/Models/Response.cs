namespace GameArch
{
    public class Response
    {
        public string Code { private set; get; }
        public string Message { private set; get; }
        
        public string Data { private set; get; }

        public Response(string code, string message, string data)
        {
            Code = code;
            Message = message;
            Data = data;
        }
    }
}