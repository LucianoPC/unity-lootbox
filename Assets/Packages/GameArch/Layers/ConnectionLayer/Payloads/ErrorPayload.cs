using System;

namespace GameArch
{
    [Serializable]
    public class ErrorPayload
    {
        public string code;
        public string message;
        
        public Error ToModel()
        {
            return new Error(code, message);
        }
    }
}