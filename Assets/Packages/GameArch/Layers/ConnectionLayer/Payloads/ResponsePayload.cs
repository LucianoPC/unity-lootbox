using System;

namespace GameArch
{
    [Serializable]
    public class ResponsePayload
    {
        public string code;
        public string message;
        
        public object data;

        public Response ToModel()
        {
            var dataText = data == null ? string.Empty : data.ToString();
            
            return new Response(code, message, dataText);
        }
    }
}