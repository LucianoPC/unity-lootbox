using System;

namespace GameArch
{
    public abstract class BaseController
    {
        ConnectionResources connectionResources;

        protected BaseController()
        {
        }

        public void BaseInitialization(ConnectionResources connectionResources)
        {
            this.connectionResources = connectionResources;
        }

        protected abstract void OnInitialize();
        protected abstract void OnFinish();

        public void Initialize()
        {
            OnInitialize();
        }

        public void Finish()
        {
            OnFinish();
        }
        
        protected void AddJsonConverter<T>(Func<string, T> converter) where T : class
        {
            var connections = connectionResources.GetAll();
            
            foreach (var connection in connections)
            {
                connection.AddConverter(converter);
            }
        }

        public Connection GetConnection(string connectionId)
        {
            return connectionResources.Get(connectionId);
        }
    }
}