namespace GameArch
{
    public class ControllerResources : Resources<BaseController>
    {
        public static ControllerResources Merge(ControllerResources a,ControllerResources b)
        {
            return Resources<BaseController>.Merge(a, b);
        }
    }
}