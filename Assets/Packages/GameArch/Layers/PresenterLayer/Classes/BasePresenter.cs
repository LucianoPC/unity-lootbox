namespace GameArch
{
    public abstract class BasePresenter
    {
        ViewResources viewResources;
        ControllerResources controllerResources;

        protected BasePresenter()
        {
        }

        public void BaseInitialization(ViewResources viewResources, ControllerResources controllerResources)
        {
            this.viewResources = viewResources;
            this.controllerResources = controllerResources;
        }

        public abstract void OnInitialize();
        
        public abstract void OnFinish();

        public void Initialize()
        {
            OnInitialize();
        }

        public void Finish()
        {
            OnFinish();
        }
        
        protected T GetController<T>(string id) where T : BaseController
        {
            return controllerResources.Get<T>(id);
        }

        protected T GetView<T>(string id) where T : IView
        {
            return viewResources.Get<T>(id);
        }
    }
}