namespace GameArch
{
    public interface IView
    {
        void OnInitialize();
        void OnFinish();
    }
}