using System;
using System.Collections.Generic;
using GameArch.Box;
using UnityEngine;

namespace GameArch.Connections.Pitaya.Box
{
    [CreateAssetMenu(fileName = "PitayaConnection", menuName = "GameArch Boxes/Connection/Pitaya/Pitaya Connection", order = 0)]
    public class PitayaConnectionBox : BaseConnectionBox
    {
#pragma warning disable 0649
        [SerializeField] PitayaConnectionInfo pitayaConnectionInfo;
#pragma warning restore 0649

        PitayaConnection pitayaConnection;

        public override Connection Connection =>
            pitayaConnection ?? (pitayaConnection = PitayaConnection.Create(pitayaConnectionInfo));

        
        [Serializable]
        class PitayaConnectionInfo : IStatusCodes
        {
#pragma warning disable 0649
            [Header("Attributes")]
            [SerializeField] List<string> successCodes;
#pragma warning restore 0649

            public List<string> SuccessCodes => successCodes;
        }
    }
}