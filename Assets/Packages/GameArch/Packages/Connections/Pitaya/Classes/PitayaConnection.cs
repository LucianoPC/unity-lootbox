namespace GameArch.Connections.Pitaya
{
    public class PitayaConnection : Connection
    {
        public static PitayaConnection Create(IStatusCodes statusCodes)
        {
            var connector = new Connector();
            var requester = new Requester(connector, statusCodes);
            var listener = new Listener(connector);
            var jsonSerializer = new JsonSerializer();
            
            return new PitayaConnection(connector, requester, listener, jsonSerializer);
        }

        PitayaConnection(
            IConnector connector, 
            IRequester requester, 
            IListener listener,
            IJsonSerializer jsonSerializer
            ) :
            
            base(
                connector, 
                requester, 
                listener, 
                jsonSerializer
            )
        {
        }
    }
}