﻿using System;
using Pitaya;

namespace GameArch.Connections.Pitaya
{
    public class Connector : IConnector
    {
        const string codeFailToConnect = "ConnectionLayer::Connector-001";
        const string codeKicked        = "ConnectionLayer::Connector-002";
        const string codeTimeout       = "ConnectionLayer::Connector-003";
        const string codeError         = "ConnectionLayer::Connector-004";
        
        const string messageFailToConnect = "Fail to connect";
        const string messageKicked        = "Kicked from server";
        const string messageTimeout       = "Timeout";
        const string messageError         = "Error";
        
        PitayaClient client;

        bool isConnecting;
        Action onConnectSuccess;
        Action<Error> onConnectError;


        public PitayaClient Client => client;

        public void Connect(string host, int port, Action onSuccess, Action<Error> onError)
        {
            CreateClient();
            
            isConnecting = true;

            onConnectSuccess = onSuccess;
            onConnectError = onError;

            client.Connect(host, port);
        }

        public void Disconnect()
        {
            if (client == null) return;
            
            client.Dispose();
            client = null;
        }
        
        void CreateClient()
        {
            if (client != null) { Disconnect(); }
            
            client = new PitayaClient();

            client.NetWorkStateChangedEvent += OnNetWorkStateChangedEvent;
        }

        void StateConnected()
        {
            CheckConnectSuccess();
        }

        void StateDisconnected()
        {
            
        }

        void StateFailToConnect()
        {
            var error = new Error(codeFailToConnect, messageFailToConnect);
            
            CheckConnectError(error);
        }

        void StateKicked()
        {
            var error = new Error(codeKicked, messageKicked);
            
            CheckConnectError(error);
        }

        void StateClosed()
        {
            
        }

        void StateConnecting()
        {
            
        }

        void StateTimeout()
        {
            var error = new Error(codeTimeout, messageTimeout);
            
            CheckConnectError(error);
        }

        void StateError()
        {
            var error = new Error(codeError, messageError);
            
            CheckConnectError(error);
        }

        void CheckConnectSuccess()
        {
            if (!isConnecting) { return; }
            isConnecting = false;
            
            onConnectSuccess?.Invoke();
        }

        void CheckConnectError(Error error)
        {
            if (!isConnecting) { return; }
            isConnecting = false;

            onConnectError?.Invoke(error);
        }

        void OnNetWorkStateChangedEvent(PitayaNetWorkState networkState, NetworkError networkError)
        {
            if (networkError == null)
            {
                networkError = new NetworkError("", "");
            }
            
            switch(networkState) {
                case PitayaNetWorkState.Connected:
                    StateConnected();
#if DEBUG_PITAYA
                    Debug.LogFormat("[PITAYA_STATE] Connected :: [error: {0}] [description: {1}]", networkError.Error, networkError.Description);
#endif
                    break;
                case PitayaNetWorkState.Disconnected:
                    StateDisconnected();
#if DEBUG_PITAYA
                    Debug.LogFormat("[PITAYA_STATE] Disconnected :: [error: {0}] [description: {1}]", networkError.Error, networkError.Description);
#endif
                    break;
                case PitayaNetWorkState.FailToConnect:
                    StateFailToConnect();
#if DEBUG_PITAYA
                    Debug.LogFormat("[PITAYA_STATE] FailToConnect :: [error: {0}] [description: {1}]", networkError.Error, networkError.Description);
#endif
                    break;
                case PitayaNetWorkState.Kicked:
                    StateKicked();
#if DEBUG_PITAYA
                    Debug.LogFormat("[PITAYA_STATE] Kicked :: [error: {0}] [description: {1}]", networkError.Error, networkError.Description);
#endif
                    break;
                case PitayaNetWorkState.Closed:
                    StateClosed();
#if DEBUG_PITAYA
                    Debug.LogFormat("[PITAYA_STATE] Closed :: [error: {0}] [description: {1}]", networkError.Error, networkError.Description);
#endif
                    break;
                case PitayaNetWorkState.Connecting:
                    StateConnecting();
#if DEBUG_PITAYA
                    Debug.LogFormat("[PITAYA_STATE] Connecting :: [error: {0}] [description: {1}]", networkError.Error, networkError.Description);
#endif
                    break;
                case PitayaNetWorkState.Timeout:
                    StateTimeout();
#if DEBUG_PITAYA
                    Debug.LogFormat("[PITAYA_STATE] Timeout :: [error: {0}] [description: {1}]", networkError.Error, networkError.Description);
#endif
                    break;
                case PitayaNetWorkState.Error:
                    StateError();
#if DEBUG_PITAYA
                    Debug.LogFormat("[PITAYA_STATE] Error :: [error: {0}] [description: {1}]", networkError.Error, networkError.Description);
#endif
                    break;
                default:
#if DEBUG_PITAYA
                    Debug.LogFormat("[PITAYA_STATE] DEFAULT OPTION :: [error: {0}] [description: {1}]", networkError.Error, networkError.Description);
#endif
                    break;
            }
        }
    }
}
