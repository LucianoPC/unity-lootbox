namespace GameArch.Connections.Pitaya
{
    public class JsonSerializer : IJsonSerializer
    {
        public string Serialize(object obj)
        {
            return SimpleJson.SimpleJson.SerializeObject(obj);
        }

        public T Deserialize<T>(string json)
        {
            return SimpleJson.SimpleJson.DeserializeObject<T>(json);
        }
    }
}