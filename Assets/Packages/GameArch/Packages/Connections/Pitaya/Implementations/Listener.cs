using System;
using Pitaya;

namespace GameArch.Connections.Pitaya
{
    public class Listener : IListener
    {
        readonly Connector connector;
        
        PitayaClient Client => connector.Client;

        public Listener(Connector connector)
        {
            this.connector = connector;
        }
        
        public void ListenRoute(string route, Action<ResponsePayload> callback)
        {
            Client.OnRoute(route,
                action: pitayaResponse =>
                {
                    var responsePayload = SimpleJson.SimpleJson.DeserializeObject<ResponsePayload>(pitayaResponse);
                    
                    callback?.Invoke(responsePayload);
                });
        }

        public void StopListenRoute(string route)
        {
            Client.OffRoute(route);
        }

        public void StopListenAllRoutes()
        {
            Client?.RemoveAllOnRouteEvents();
        }
    }
}