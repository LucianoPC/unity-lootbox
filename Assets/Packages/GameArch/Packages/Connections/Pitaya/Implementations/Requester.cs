using System;
using Pitaya;

namespace GameArch.Connections.Pitaya
{
    public class Requester : IRequester
    {
        readonly Connector connector;
        
        readonly IStatusCodes statusCodes;
        
        PitayaClient Client => connector.Client;

        public Requester(Connector connector, IStatusCodes statusCodes)
        {
            this.connector = connector;
            this.statusCodes = statusCodes;
        }

        public void SendRequest(string route, string requestData, Action<ResponsePayload> onSuccess,
            Action<ErrorPayload> onError)
        {
            Client.Request(route, requestData, 
                action: pitayaResponse =>
                {
                    var responsePayload = SimpleJson.SimpleJson.DeserializeObject<ResponsePayload>(pitayaResponse);
                    var isSuccessCode = statusCodes.SuccessCodes.Contains(responsePayload.code);

                    if (isSuccessCode)
                    {
                        onSuccess?.Invoke(responsePayload);
                    }
                    else
                    {
                        var errorResponsePayload = new ErrorPayload
                        {
                            code = responsePayload.code,
                            message = responsePayload.message,
                        };
                        
                        onError?.Invoke(errorResponsePayload);
                    }
                },
                errorAction: pitayaError =>
                {
                    var errorResponsePayload = new ErrorPayload
                    {
                        code = pitayaError.Code,
                        message = pitayaError.Msg,
                    };
                    
                    onError?.Invoke(errorResponsePayload);
                });
        }

        public void ClearAllCallbacks()
        {
            Client?.ClearAllCallbacks();
        }
    }
}