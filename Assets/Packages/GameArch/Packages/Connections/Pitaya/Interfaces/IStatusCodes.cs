using System.Collections.Generic;

namespace GameArch.Connections.Pitaya
{
    public interface IStatusCodes
    {
        List<string> SuccessCodes { get; }        
    }
}