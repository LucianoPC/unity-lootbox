namespace GameArch.Connections.Pitaya
{
    public class Player
    {
        public string Name { private set; get; }
        public string UserId { private set; get; }

        public Player(string name, string userId)
        {
            Name = name;
            UserId = userId;
        }
    }
}