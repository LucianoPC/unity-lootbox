using System;
using GameArch.Box;
using UnityEngine;

namespace GameArch.Controllers.Authentication.Box
{
    [CreateAssetMenu(fileName = "AuthenticationController", menuName = "GameArch Boxes/Controller/Authentication/Authentication Controller", order = 0)]
    public class AuthenticationControllerBox : BaseControllerBox
    {
#pragma warning disable 0649
        [SerializeField] AuthenticationControllerInfo authenticationControllerInfo;
#pragma warning restore 0649

        AuthenticationController authenticationController;

        public override BaseController Controller =>
            authenticationController ??
            (authenticationController = new AuthenticationController(authenticationControllerInfo));
        
        
        [Serializable]
        class AuthenticationControllerInfo : IAuthenticationControllerInfo
        {
#pragma warning disable 0649
            [Header("Dependencies")]
            [SerializeField] BaseConnectionBox connection;
            
            [Header("Attributes")]
            [SerializeField] string authenticationRoute;
#pragma warning restore 0649

            public string ConnectionId => connection.Id;
            
            public string AuthenticationRoute => authenticationRoute;
        }
    }
}