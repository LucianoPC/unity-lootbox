using System;

namespace GameArch.Controllers.Authentication
{
    public class AuthenticationController : BaseController
    {
        readonly IAuthenticationControllerInfo info;
        
        bool isAuthenticated;
        AuthenticationData authenticationData;
        AuthenticationStorage storage;
        
        Connection connection;
        
        public AuthenticationController(IAuthenticationControllerInfo info)
        {
            this.info = info;
            
            storage = new AuthenticationStorage();
        }

        public bool IsAuthenticated => isAuthenticated;
        
        public AuthenticationData AuthenticationData => authenticationData;
        
        public event Action Authenticating;
        public event Action<AuthenticationData> AuthenticationSucceeded;
        public event Action<Error> AuthenticationFailed;

        protected override void OnInitialize()
        {
            connection = GetConnection(info.ConnectionId);
            
            authenticationData = storage.Load();
            
            AddJsonConverter(AuthenticationDataJsonConverter.FromJson);
        }

        protected override void OnFinish()
        {
        }

        public void Authenticate()
        {
            Authenticating?.Invoke();
            
            if (isAuthenticated)
            {
                OnAuthenticateSuccess(authenticationData);
                return;
            }

            var request = new { userId = authenticationData.UserId };
            
            connection.SendRequest<AuthenticationData>(info.AuthenticationRoute, request, OnAuthenticateSuccess, OnAuthenticateError); 
        }

        void OnAuthenticateSuccess(AuthenticationData authenticationData)
        {
            isAuthenticated = true;
            this.authenticationData = authenticationData;
            
            storage.Save(authenticationData);
            
            AuthenticationSucceeded?.Invoke(authenticationData);
        }

        void OnAuthenticateError(Error error)
        {
            AuthenticationFailed?.Invoke(error);
        }
    }
}