namespace GameArch.Controllers.Authentication
{
    public class AuthenticationStorage : Storage<AuthenticationData>
    {
        const string storageKey = "GameArch.Controllers.Authentication.AuthenticationStorage";

        public AuthenticationStorage() : base(storageKey, GetDefaultData(), AuthenticationDataJsonConverter.ToJson,
            AuthenticationDataJsonConverter.FromJson)
        {
        }

        static AuthenticationData GetDefaultData()
        {
            var userId = string.Empty;
            
            return new AuthenticationData(userId);
        }
    }
}