namespace GameArch.Controllers.Authentication
{
    public interface IAuthenticationControllerInfo
    {
        string ConnectionId { get; }
        string AuthenticationRoute { get; }
    }
}