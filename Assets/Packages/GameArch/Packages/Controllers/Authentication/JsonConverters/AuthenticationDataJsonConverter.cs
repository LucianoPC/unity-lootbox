using System;

namespace GameArch.Controllers.Authentication
{
    public static class AuthenticationDataJsonConverter
    {
        public static AuthenticationData FromJson(string json)
        {
            var payload = SimpleJson.SimpleJson.DeserializeObject<AuthenticationDataPayload>(json);
            
            return new AuthenticationData(payload.userId);
        }

        public static string ToJson(AuthenticationData data)
        {
            var payload = new AuthenticationDataPayload
            {
                userId = data.UserId,
            };

            return SimpleJson.SimpleJson.SerializeObject(payload);
        }

        [Serializable]
        class AuthenticationDataPayload
        {
            public string userId = "";
        }
    }
}