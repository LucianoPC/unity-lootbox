namespace GameArch.Controllers.Authentication
{
    public class AuthenticationData
    {
        public string UserId { private set; get; }

        public AuthenticationData(string userId)
        {
            UserId = userId;
        }
    }
}