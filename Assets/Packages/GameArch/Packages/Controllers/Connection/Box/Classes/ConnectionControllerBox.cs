using System;
using GameArch.Box;
using UnityEngine;

namespace GameArch.Controllers.Connection.Box
{
    [CreateAssetMenu(fileName = "ConnectionController", menuName = "GameArch Boxes/Controller/Connection/Connection Controller", order = 0)]
    public class ConnectionControllerBox : BaseControllerBox
    {
#pragma warning disable 0649
        [SerializeField] ConnectionControllerInfo connectionControllerInfo;
#pragma warning restore 0649

        ConnectionController connectionController;

        public override BaseController Controller => 
            connectionController ??
            (connectionController = new ConnectionController(connectionControllerInfo));
        
        
        [Serializable]
        class ConnectionControllerInfo : IConnectionControllerInfo
        {
#pragma warning disable 0649
            [Header("Dependencies")]
            [SerializeField] BaseConnectionBox connection;
            
            [Header("Attributes")]
            [SerializeField] string serverHost;
            [SerializeField] int serverPort;
#pragma warning restore 0649

            public string ConnectionId => connection.Id;
            
            public string ServerHost => serverHost;
            public int ServerPort => serverPort;
        }
    }
}