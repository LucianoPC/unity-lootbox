using System;

namespace GameArch.Controllers.Connection
{
    public class ConnectionController : BaseController
    {
        readonly IConnectionControllerInfo info;

        GameArch.Connection connection;
        
        public bool IsConnected { private set; get; }
            
        public ConnectionController(IConnectionControllerInfo info)
        {
            this.info = info;
        }

        public event Action Connecting;
        public event Action ConnectionSucceeded;
        public event Action<Error> ConnectionFailed;
        
        protected override void OnInitialize()
        {
            connection = GetConnection(info.ConnectionId);
        }

        protected override void OnFinish()
        {
        }

        public void Connect()
        {
            Connecting?.Invoke();
            
            connection.Connect(info.ServerHost, info.ServerPort, OnConnectSuccess, OnConnectError);
        }

        void OnConnectSuccess()
        {
            IsConnected = true;
            ConnectionSucceeded?.Invoke();
        }

        void OnConnectError(Error error)
        {
            ConnectionFailed?.Invoke(error);
        }

        public void Disconnect()
        {
            connection.Disconnect();
        }
    }
}