namespace GameArch.Controllers.Connection
{
    public interface IConnectionControllerInfo
    {
        string ConnectionId { get; }
        string ServerHost { get; }
        int ServerPort { get; }
    }
}