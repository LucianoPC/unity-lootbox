using System;
using GameArch.Box;
using GameArch.Controllers.Authentication.Box;
using GameArch.Controllers.Connection.Box;
using UnityEngine;

namespace GameArch.Presenters.Authentication.Box
{
    public class AuthenticationPresenterBox : BasePresenterBox
    {
#pragma warning disable 0649
        [SerializeField] AuthenticationPresenterInfo authenticationPresenterInfo;
#pragma warning restore 0649

        AuthenticationPresenter authenticationPresenter;

        public override BasePresenter Presenter =>
            authenticationPresenter ??
            (authenticationPresenter = new AuthenticationPresenter(authenticationPresenterInfo));
        
        
        [Serializable]
        public class AuthenticationPresenterInfo : IAuthenticationPresenterInfo
        {
#pragma warning disable 0649
            [Header("Dependencies")]
            [SerializeField] ConnectionControllerBox connectionController;
            [SerializeField] AuthenticationControllerBox authenticationController;
            [SerializeField] BaseViewBox alertView;
            [SerializeField] BaseViewBox dialogView;
            
            [Header("Attributes")]
            [SerializeField] string authenticatingMessage;
            [SerializeField] string retryAuthenticationMessage;
            [SerializeField] ErrorList authenticationError;
#pragma warning restore 0649
                
            public string ConnectionControllerId => connectionController.Id;
            public string AuthenticationControllerId => authenticationController.Id;
            public string AlertViewId => alertView.Id;
            public string DialogViewId => dialogView.Id;

            public string AuthenticatingMessage => authenticatingMessage;
            public string RetryAuthenticationMessage => retryAuthenticationMessage;
            
            public string GetAuthenticationErrorMessage(Error error)
            {
                foreach (var errorPair in authenticationError.ErrorPairs)
                {
                    if (error.Code == errorPair.ErrorCode)
                    {
                        return errorPair.ErrorMessage;
                    }
                }
                
                return authenticationError.DefaultErrorMessage;
            }
            
            [Serializable]
            class ErrorList
            {
#pragma warning disable 0649
                public ErrorPair[] ErrorPairs;
                public string DefaultErrorMessage;
#pragma warning restore 0649
            }

            [Serializable]
            class ErrorPair
            {
#pragma warning disable 0649
                public string ErrorCode;
                public string ErrorMessage;
#pragma warning restore 0649
            }
        }
    }
}