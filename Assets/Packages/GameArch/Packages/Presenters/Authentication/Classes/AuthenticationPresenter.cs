using GameArch.Controllers.Authentication;
using GameArch.Controllers.Connection;

namespace GameArch.Presenters.Authentication
{
    public class AuthenticationPresenter : BasePresenter
    {
        readonly IAuthenticationPresenterInfo info;
        
        ConnectionController connectionController;
        AuthenticationController authenticationController;

        IAlertView alertView;
        IDialogView dialogView;

        public AuthenticationPresenter(IAuthenticationPresenterInfo info)
        {
            this.info = info;
        }

        public override void OnInitialize()
        {
            connectionController = GetController<ConnectionController>(info.ConnectionControllerId);
            authenticationController = GetController<AuthenticationController>(info.AuthenticationControllerId);

            alertView = GetView<IAlertView>(info.AlertViewId);
            dialogView = GetView<IDialogView>(info.DialogViewId);
            
            RegisterCallbacks();
        }

        public override void OnFinish()
        {
            UnRegisterCallbacks();
        }

        void RegisterCallbacks()
        {
            authenticationController.Authenticating += OnAuthenticating;
            authenticationController.AuthenticationSucceeded += OnAuthenticationSucceeded;
            authenticationController.AuthenticationFailed += OnAuthenticationFailed;
        }

        void UnRegisterCallbacks()
        {
            authenticationController.Authenticating -= OnAuthenticating;
            authenticationController.AuthenticationSucceeded -= OnAuthenticationSucceeded;
            authenticationController.AuthenticationFailed -= OnAuthenticationFailed;
        }
        
        void OnAuthenticating()
        {
            alertView.Show(info.AuthenticatingMessage);
        }

        void OnAuthenticationSucceeded(AuthenticationData authenticationData)
        {
            alertView.Hide();
        }

        void OnAuthenticationFailed(Error error)
        {
            var errorMessage = info.GetAuthenticationErrorMessage(error);
            
            dialogView.Show(errorMessage, info.RetryAuthenticationMessage, 
                onConfirm: () =>
                {
                    dialogView.Hide();
                    connectionController.Connect();
                });
        }
    }
}