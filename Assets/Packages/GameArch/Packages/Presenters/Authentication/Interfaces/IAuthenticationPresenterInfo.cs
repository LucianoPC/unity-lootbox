namespace GameArch.Presenters.Authentication
{
    public interface IAuthenticationPresenterInfo
    {
        string ConnectionControllerId { get; }
        string AuthenticationControllerId { get; }
        
        string AlertViewId { get; }
        string DialogViewId { get; }
        
        string AuthenticatingMessage { get; }
        string RetryAuthenticationMessage { get; }

        string GetAuthenticationErrorMessage(Error error);
    }
}