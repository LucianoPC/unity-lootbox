namespace GameArch.Presenters.Authentication
{
    public interface IAlertView : IView
    {
        void Show(string message);
        void Hide();
    }
}