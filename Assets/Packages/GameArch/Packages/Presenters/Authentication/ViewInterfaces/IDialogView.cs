using System;

namespace GameArch.Presenters.Authentication
{
    public interface IDialogView : IView
    {
        void Show(string message, string confirmationText, Action onConfirm);
        void Hide();
    }
}