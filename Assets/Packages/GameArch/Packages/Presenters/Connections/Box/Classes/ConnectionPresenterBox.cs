using System;
using GameArch.Box;
using GameArch.Controllers.Connection.Box;
using UnityEngine;

namespace GameArch.Presenters.Connection.Box
{
    public class ConnectionPresenterBox : BasePresenterBox
    {
#pragma warning disable 0649
        [SerializeField] ConnectionPresenterInfo connectionPresenterInfo;
#pragma warning restore 0649

        ConnectionPresenter connectionPresenter;

        public override BasePresenter Presenter =>
            connectionPresenter ??
            (connectionPresenter = new ConnectionPresenter(connectionPresenterInfo));
        
        
        [Serializable]
        class ConnectionPresenterInfo : IConnectionPresenterInfo
        {
#pragma warning disable 0649
            [Header("Dependencies")]
            [SerializeField] ConnectionControllerBox connectionController;
            [SerializeField] BaseViewBox alertView;
            [SerializeField] BaseViewBox dialogView;
            
            [Header("Attributes")]
            [SerializeField] string connectingMessage;
            [SerializeField] string retryConnectionMessage;
            [SerializeField] ErrorList connectionError;
#pragma warning restore 0649

            public string ConnectionControllerId => connectionController.Id;
            public string AlertViewId => alertView.Id;
            public string DialogViewId => dialogView.Id;

            public string ConnectingMessage => connectingMessage;
            public string RetryConnectionMessage => retryConnectionMessage;
            
            public string GetConnectionErrorMessage(Error error)
            {
                foreach (var errorPair in connectionError.ErrorPairs)
                {
                    if (error.Code == errorPair.ErrorCode)
                    {
                        return errorPair.ErrorMessage;
                    }
                }
                
                return connectionError.DefaultErrorMessage;
            }

            [Serializable]
            class ErrorList
            {
#pragma warning disable 0649
                public ErrorPair[] ErrorPairs;
                public string DefaultErrorMessage;
#pragma warning restore 0649
            }

            [Serializable]
            class ErrorPair
            {
#pragma warning disable 0649
                public string ErrorCode;
                public string ErrorMessage;
#pragma warning restore 0649
            }
        }
    }
}