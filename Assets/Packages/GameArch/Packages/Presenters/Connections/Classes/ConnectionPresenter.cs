using GameArch.Controllers.Connection;

namespace GameArch.Presenters.Connection
{
    public class ConnectionPresenter : BasePresenter
    {
        readonly IConnectionPresenterInfo info;
        
        ConnectionController connectionController;

        IAlertView alertView;
        IDialogView dialogView;

        public ConnectionPresenter(IConnectionPresenterInfo info)
        {
            this.info = info;
        }

        public override void OnInitialize()
        {
            connectionController = GetController<ConnectionController>(info.ConnectionControllerId);
            
            alertView = GetView<IAlertView>(info.AlertViewId);
            dialogView = GetView<IDialogView>(info.DialogViewId);
            
            RegisterCallbacks();
        }

        public override void OnFinish()
        {
            UnRegisterCallbacks();
        }

        void RegisterCallbacks()
        {
            connectionController.Connecting += OnConnecting;
            connectionController.ConnectionSucceeded += OnConnectionSucceeded;
            connectionController.ConnectionFailed += OnConnectionFailed;
        }

        void UnRegisterCallbacks()
        {
            connectionController.Connecting -= OnConnecting;
            connectionController.ConnectionSucceeded -= OnConnectionSucceeded;
            connectionController.ConnectionFailed -= OnConnectionFailed;
        }
        
        void OnConnecting()
        {
            alertView.Show(info.ConnectingMessage);
        }

        void OnConnectionSucceeded()
        {
            alertView.Hide();
        }

        void OnConnectionFailed(Error error)
        {
            var errorMessage = info.GetConnectionErrorMessage(error);
            
            dialogView.Show(errorMessage, info.RetryConnectionMessage, 
                onConfirm: () =>
                {
                    dialogView.Hide();
                    connectionController.Connect();
                });
        }
    }
}