namespace GameArch.Presenters.Connection
{
    public interface IConnectionPresenterInfo
    {
        string ConnectionControllerId { get; }
        
        string AlertViewId { get; }
        string DialogViewId { get; }
        
        string ConnectingMessage { get; }
        string RetryConnectionMessage { get; }

        string GetConnectionErrorMessage(Error error);
    }
}