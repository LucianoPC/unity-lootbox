namespace GameArch.Presenters.Connection
{
    public interface IAlertView : IView
    {
        void Show(string message);
        void Hide();
    }
}