namespace GameArch
{
    public static class IdGenerator
    {
        const int smallIdLength = 8;
        
        public static string NewSmallId()
        {
            var id = System.Guid.NewGuid().ToString("N").Substring(0, smallIdLength);

            return id;
        }
        
        public static string NewId()
        {
            var id = System.Guid.NewGuid().ToString("D");

            return id;
        }
    }
}