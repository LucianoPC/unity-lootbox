using System.Collections.Generic;
using UnityEngine;

namespace GameArch
{
    public class Resources<T> where T : class
    {
        List<T> list;
        Dictionary<string, T> map;

        public Resources()
        {
            list = new List<T>();
            map = new Dictionary<string, T>();
        }
        
        public void Add(string id, T obj)
        {
            if (map.ContainsKey(id))
            {
                Debug.LogErrorFormat("Resource of {1} on Add: id {0} already exists", typeof(T), id);
                return;
            }
            
            map.Add(id, obj);
            list.Add(obj);
        }
        
        public T Get(string id)
        {
            if (!map.ContainsKey(id))
            {
                Debug.LogErrorFormat("Resource of {1} on Get: id {0} not exists", typeof(T), id);
                return null;
            }
            
            var connection = map[id];

            return connection;
        }

        public TK Get<TK>(string id) where TK : T
        {
            var obj = Get(id);

            return (TK) obj;
        }

        public List<T> GetAll()
        {
            return list;
        }

        public static Resources<T> Merge(Resources<T> a, Resources<T> b)
        {
            return Merge<Resources<T>>(a, b);
        }

        public static TK Merge<TK>(TK a, TK b) where TK : Resources<T>, new()
        {
            var resources = new TK();

            foreach (var pair in a.map)
            {
                resources.Add(pair.Key, pair.Value);
            }
            
            foreach (var pair in b.map)
            {
                resources.Add(pair.Key, pair.Value);
            }

            return resources;
        }
    }
}