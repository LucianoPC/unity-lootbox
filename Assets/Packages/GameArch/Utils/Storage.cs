using System;
using UnityEngine;

namespace GameArch
{
    public class Storage<T> where T : class
    {
        T data;
        
        readonly T defaultData;
        readonly string key;

        readonly Func<T, string> toJsonFunc;
        readonly Func<string, T> fromJsonFunc;

        public Storage(string key, T defaultData, Func<T, string> toJsonFunc, Func<string, T> fromJsonFunc)
        {
            this.key = key;
            this.defaultData = defaultData;
            this.toJsonFunc = toJsonFunc;
            this.fromJsonFunc = fromJsonFunc;
        }
        
        public void Save(T data)
        {
            this.data = data;

            var dataJson = toJsonFunc(data);
            
            PlayerPrefs.SetString(key, dataJson);
            PlayerPrefs.Save();
        }
        
        public T Load()
        {
            var dataJson = PlayerPrefs.GetString(key, string.Empty);

            if (string.IsNullOrEmpty(dataJson)) { return defaultData; }

            data = fromJsonFunc(dataJson);

            return data ?? defaultData;
        }
    }
}